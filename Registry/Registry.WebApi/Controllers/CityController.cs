﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Registry.Domain.Interfaces.Services;
using Registry.Models.Cities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Registry.WebApi.Controllers
{
	public class CityController : ControllerBase
	{
		private readonly ICityService _cityService;
		private readonly IMapper _mapper;

		public CityController(ICityService cityService, IMapper mapper)
		{
			_cityService = cityService;
			_mapper = mapper;
		}

		[HttpGet(nameof(GetCities))]
		public IEnumerable<CityModel> GetCities()
		{
			var data = _cityService.GetCities();
			var model = data.Select(x => _mapper.Map<CityModel>(x)).ToList();
			return model;
		}

		[HttpPost(nameof(SaveCity))]
		public async Task<CityModel> SaveCity(CityModel model)
		{
			var result = await _cityService.SaveCityAsync(model.ID, model.Name);
			model = _mapper.Map<CityModel>(result);
			return model;
		}

		[HttpPost(nameof(DeleteCity))]
		public async Task DeleteCity(int id)
		{
			await _cityService.DeleteCityAsync(id);
		}
	}
}