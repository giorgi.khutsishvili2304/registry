﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Registry.Domain;
using Registry.Domain.Enumerations;
using Registry.Domain.Interfaces.Data;
using Registry.Domain.Interfaces.Services;
using Registry.Domain.Reports;
using Registry.Models.Persons;
using Registry.Models.Relations;
using Registry.Models.Resources;

namespace Registry.WebApi.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class PersonController : ControllerBase
	{
		private readonly IPersonService _personService;
		private readonly IMapper _mapper;

		public PersonController(IPersonService personService, IMapper mapper)
		{
			_personService = personService;
			_mapper = mapper;
		}

		[HttpGet(nameof(GetPerson))]
		public async Task<PersonModel> GetPerson(int id)
		{
			var person = await _personService.GetAsync(id);
			var model = _mapper.Map<PersonModel>(person);

			model.ContactInfos = person.ContactInfos.Select(x => _mapper.Map<ContactInfoModel>(x)).ToList();
			model.PersonImages = person.PersonImages.Select(x => _mapper.Map<PersonImageModel>(x)).ToList();
			model.Relations = person.Side1Relations.Select(x => _mapper.Map<RelationModel>(x)).ToList();

			return model;
		}

		[HttpPost(nameof(SavePerson))]
		public async Task<PersonBaseModel> SavePerson(PersonBaseModel model)
		{
			var person = _mapper.Map<Person>(model);
			var result = await _personService.SavePersonAsync(person);
			model = _mapper.Map<Person, PersonBaseModel>(result);
			return model;
		}

		[HttpPost(nameof(DeletePerson))]
		public async Task DeletePerson(int id)
		{
			await _personService.DeletePersonAsync(id);
		}

		[HttpPost(nameof(SavePersonContactInfo))]
		public async Task<ContactInfoModel> SavePersonContactInfo(ContactInfoModel model)
		{
			var data = _mapper.Map<ContactInfo>(model);
			var result = await _personService.SavePersonContactInfoAsync(data);
			model = _mapper.Map<ContactInfoModel>(result);
			return model;
		}

		[HttpPost(nameof(DeletePersonContactInfo))]
		public async Task DeletePersonContactInfo(int id)
		{
			await _personService.DeletePersonContactInfoAsync(id);
		}

		[HttpPost(nameof(SearchPersonsBasic))]
		public List<PersonBaseModel> SearchPersonsBasic(BasicSearchModel model)
		{
			ValidateFilter(model);
			var data = _personService.SearchPersonsBasic(model.Firstname, model.Lastname, model.PersonalNumber, model.PageSize, model.Page);
			var result = data.Select(x => _mapper.Map<PersonBaseModel>(x)).ToList();
			return result;
		}

		[HttpPost(nameof(SearchPersonsAdvanced))]
		public List<PersonBaseModel> SearchPersonsAdvanced(AdvancedSearchModel model)
		{
			ValidateFilter(model);
			var data = _personService.SearchPersonsAdvanced(model.Firstname, model.Lastname, model.PersonalNumber, model.BirthDateFrom,
				model.BirthDateTo, model.GenderType, model.
				CityID, model.PageSize, model.Page);
			var result = data.Select(x => _mapper.Map<PersonBaseModel>(x)).ToList();
			return result;
		}

		[HttpGet(nameof(GetPersonReportByRelationsReport))]
		public Task<List<PersonRelationReportModel>> GetPersonReportByRelationsReport()
		{
			return _personService.ReportForPersonsByRelation();
		}

		private void ValidateFilter(BasicSearchModel filter)
		{
			if (filter.Page < 1 || filter.PageSize < 1)
				throw new Registry.Services.Validation.CustomValidationException(ErrorCodes.PAGING_PARAMETERS_INCORRECT, Resources.PAGING_PARAMETERS_INCORRECT);
		}
	}
}