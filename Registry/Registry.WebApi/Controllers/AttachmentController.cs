﻿using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Registry.Domain.Enumerations;
using Registry.Domain.Interfaces.Services;
using Registry.Models.Cities;
using Registry.Models.Persons;
using Registry.Models.Resources;
using Registry.Services.Validation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Registry.WebApi.Controllers
{
    public class AttachmentController : ControllerBase
    {
        private readonly IPersonService _personService;
        private readonly IWebHostEnvironment _env;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly string _uploadsFolderName;

        public AttachmentController(IPersonService personService, IMapper mapper, IWebHostEnvironment env, IConfiguration configuration)
        {
            _personService = personService;
            _env = env;
            _mapper = mapper;
            _configuration = configuration;
            _uploadsFolderName = _configuration["UploadsDirectoryName"] ?? "Uploads";
        }

        [HttpPost(nameof(UploadPersonImage))]
        public async Task UploadPersonImage(int personId, IFormFile file)
        {
            if (file == null || file.Length == 0)
                throw new CustomValidationException(ErrorCodes.FILE_IS_REQUIRED, Resources.FILE_IS_REQUIRED);
            else if (file.Length > int.Parse(_configuration["UploadFileSizeLimit"] ?? "5242880"))
                throw new CustomValidationException(ErrorCodes.MAX_FILE_SIZE_LIMIT, Resources.MAX_FILE_SIZE_LIMIT);

            await _personService.SavePersonImage(personId, DateTime.UtcNow.Ticks.ToString(), file.ContentType, new DirectoryInfo(_env.ContentRootPath), _uploadsFolderName, file.OpenReadStream());
        }

        [HttpGet(nameof(GetPersonImages))]
        public List<PersonImageModel> GetPersonImages(int personID)
        {
            var data = _personService.GetPersonImages(personID);
            var model = data.Select(x => _mapper.Map<PersonImageModel>(x)).ToList();
            return model;
        }

        [HttpGet(nameof(GetPersonImage))]
        public async Task<IActionResult> GetPersonImage(int id)
        {
            var img = await _personService.GetPersonImageAsync(id);
            if (img == null) throw new CustomValidationException(ErrorCodes.FILE_NOT_FOUND, Resources.FILE_NOT_FOUND);

            var filename = Path.Combine(_env.ContentRootPath, img.Filename);
            if (!System.IO.File.Exists(filename)) return NotFound();

            var memoryStream = new MemoryStream();
            using (var stream = new FileStream(filename, FileMode.Open))
            {
                await stream.CopyToAsync(memoryStream);
                memoryStream.Position = 0;
                return File(memoryStream, img.ContentType);
            }
        }

        [HttpPost(nameof(DeletePersonImage))]
        public async Task DeletePersonImage(int id)
        {
            var img = await _personService.GetPersonImageAsync(id);
            var filename = Path.Combine(_env.ContentRootPath, img?.Filename);

            System.IO.File.Delete(filename);
            await _personService.DeletePersonImageAsync(id);
        }
    }
}