﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Registry.Domain;
using Registry.Domain.Interfaces.Data;
using Registry.Domain.Interfaces.Services;
using Registry.Models.Persons;
using Registry.Models.Relations;

namespace Registry.WebApi.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class RelationController : ControllerBase
	{
		private readonly IRelationService _relationService;
		private readonly IMapper _mapper;

		public RelationController(IRelationService relationService, IMapper mapper)
		{
			_relationService = relationService;
			_mapper = mapper;
		}

		[HttpGet(nameof(GetRelationTypes))]
		public IEnumerable<RelationTypeModel> GetRelationTypes()
		{
			var data = _relationService.GetRelationTypes();
			var model = data.Select(x => _mapper.Map<RelationTypeModel>(x)).ToList();
			return model;
		}

		[HttpPost(nameof(SaveRelationType))]
		public async Task<RelationTypeModel> SaveRelationType(RelationTypeModel model)
		{
			var result = await _relationService.SaveRelationType(model.ID, model.Name);
			model = _mapper.Map<RelationType, RelationTypeModel>(result);
			return model;
		}

		[HttpPost(nameof(SavePersonRelation))]
		public async Task<RelationModel> SavePersonRelation(int person1ID, int person2ID, int relationTypeID)
		{
			var result = await _relationService.SavePersonRelationAsync(person1ID, person2ID, relationTypeID);
			var model = _mapper.Map<RelationModel>(result);
			return model;
		}

		[HttpPost(nameof(DeletePersonRelation))]
		public void DeletePersonRelation(int person1ID, int person2ID)
		{
			_relationService.DeletePersonRelation(person1ID, person2ID);
		} 
	}
}