﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Registry.Repositories.Context;
using Registry.WebApi.Filters;
using Microsoft.OpenApi.Models;
using Registry.Infrastructure.DI;
using Autofac;
using Registry.WebApi.Middleware;
using Autofac.Extensions.DependencyInjection;
using Registry.Domain.Interfaces.Core;
using System.Globalization;
using Microsoft.AspNetCore.Localization;
using AutoMapper;
using Registry.Models.Mapper;

namespace Registry.WebApi
{
	public class Startup
	{
		public Startup(IWebHostEnvironment env)
		{
			var builder = new ConfigurationBuilder()
					.SetBasePath(env.ContentRootPath)
					.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
					.AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
					.AddEnvironmentVariables();

			Configuration = builder.Build();
		}

		public IConfiguration Configuration { get; }
		public ILifetimeScope AutofacContainer { get; private set; }

		public void ConfigureServices(IServiceCollection services)
		{
			services.AddControllers(options =>
			{
				options.Filters.Add(new ValidateModelAttribute());
			});//.ConfigureApiBehaviorOptions(opt=> opt.;

			services.AddSwaggerGen(c =>
			{
				c.SwaggerDoc("v1", new OpenApiInfo { Title = "Person API", Version = "V1" });
				c.OperationFilter<SwaggerOperationFilter>();
			});


			services.Configure<ApiBehaviorOptions>(options =>
			{
				options.SuppressModelStateInvalidFilter = true;
			});

			var mappingConfig = new MapperConfiguration(config =>
			{
				config.AddProfile(new MappingProfile());
			});

			IMapper mapper = mappingConfig.CreateMapper();
			services.AddSingleton(mapper);

			services.AddAutoMapper(typeof(Domain.Person).Assembly, typeof(Models.Persons.PersonModel).Assembly);
		}

		public void ConfigureContainer(ContainerBuilder builder)
		{
			builder.RegisterModule(new RegistryInjectionModule());
		}

		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			this.AutofacContainer = app.ApplicationServices.GetAutofacRoot();

			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}

			app.UseSwagger();
			app.UseSwaggerUI(c =>
			{
				c.SwaggerEndpoint("/swagger/v1/swagger.json", "Person API V1");
				c.RoutePrefix = string.Empty;
			});

			app.UseRouting();
			app.UseLoggingMiddleware();

			var supportedCultures = new[]
			{
					new CultureInfo("en-US"),
					new CultureInfo("ka-GE"),
			};

			app.UseRequestLocalization(new RequestLocalizationOptions
			{
				DefaultRequestCulture = new RequestCulture("en-US"),
				SupportedCultures = supportedCultures,
				SupportedUICultures = supportedCultures
			});


			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllers();
			});
		}
	}
}