﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Registry.Services.Validation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Registry.WebApi.Middleware
{
	public class ExceptionLoggingMiddleware
	{
		private readonly RequestDelegate _next;
		private readonly ILogger<Program> _logger;

		public ExceptionLoggingMiddleware(RequestDelegate next, ILogger<Program> logger)
		{
			_next = next;
			_logger = logger;
		}

		public async Task Invoke(HttpContext httpContext)
		{
			var request = httpContext.Request;
			try
			{
				await _next(httpContext);
				var response = httpContext.Response;
			}
			catch (CustomValidationException ex)
			{
				await ReturnResponseAsync(JsonConvert.SerializeObject(new
				{
					ex.ErrorCode,
					ex.Message,
				}), HttpStatusCode.BadRequest, httpContext);
			}
			catch (Exception ex) when (!(ex is ValidationException))
			{
				Guid uid = Guid.NewGuid();
				string message = $"Error occured, Method:{request.Method}, Path:{request.Path}, UID: {uid}";
				_logger.LogError(ex, message);

				await ReturnResponseAsync(message, HttpStatusCode.InternalServerError, httpContext);
			}
		}



		public async Task ReturnResponseAsync(string message, HttpStatusCode errorCode, HttpContext httpContext)
		{
			var result = JsonConvert.SerializeObject(new { message });
			httpContext.Response.ContentType = "application/json";
			httpContext.Response.StatusCode = (int)errorCode;
			await httpContext.Response.WriteAsync(result);
		}
	}

	public static class LoggingMiddlewareExtension
	{
		public static IApplicationBuilder UseLoggingMiddleware(this IApplicationBuilder builder)
		{
			return builder.UseMiddleware<ExceptionLoggingMiddleware>();
		}
	}
}