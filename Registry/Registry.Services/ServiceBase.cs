﻿using Registry.Domain.Interfaces.Core;
using Registry.Domain.Interfaces.Data;
using Registry.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Registry.Services
{
	public class ServiceBase<T, K> : IService<T>
		where T : class, new()
		where K : IRepository<T>
	{

		protected IUnitOfWork _context;
		protected K _repository;

		public ServiceBase(IUnitOfWork context, K repository)
		{
			_context = context ?? throw new ArgumentNullException("context");
			_repository = repository ?? throw new ArgumentNullException("repository");
		}

		public async Task<T> GetAsync(int id)
		{
			return await _repository.GetAsync(id);
		}

		public IQueryable<T> Set()
		{
			return _repository.Set();
		}

		public async Task SaveAsync(T entity)
		{
			await _repository.SaveAsync(entity);
			await _context.CommitAsync();
		}

		public async Task DeleteAsync(int id)
		{
			await _repository.DeleteAsync(id);
			await _context.CommitAsync();
		}

		public async Task DeleteAsync(T entity)
		{
			_repository.Delete(entity);
			await _context.CommitAsync();
		}
	}
}