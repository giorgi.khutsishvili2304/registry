﻿using Registry.Domain;
using Registry.Domain.Interfaces.Core;
using Registry.Domain.Interfaces.Data;
using Registry.Domain.Interfaces.Services;
using Registry.Models.Persons;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using Registry.Models.Resources;
using System.Resources;
using Registry.Services.Validation;
using Registry.Domain.Enumerations;
using System.Threading.Tasks;

namespace Registry.Services
{
	public class RelationService : ServiceBase<Relation, IRelationRepository>, IRelationService
	{
		private readonly IRelationTypeRepository _relationTypeRepository;
		private readonly IRelationRepository _relationRepository;

		public RelationService(IUnitOfWork context,
			IRelationRepository repository,
			IRelationTypeRepository relationTypeRepository,
			IRelationRepository relationRepository
			) : base(context, repository)
		{
			_relationTypeRepository = relationTypeRepository;
			_relationRepository = relationRepository;
		}

		/// <summary>
		/// Assume that person may have only one relationtype with another person
		/// </summary>
		/// <param name="person1Id"></param>
		/// <param name="person2Id"></param>
		/// <param name="relationTypeID"></param>
		/// <returns></returns>
		public async Task<Relation> SavePersonRelationAsync(int person1Id, int person2Id, int relationTypeID)
		{
			using (var transaction = await _context.BeginTransactionAsync())
			{
				//check relation type existance
				var relationType = await _relationTypeRepository.GetAsync(relationTypeID);
				if (relationType == null) throw new CustomValidationException(ErrorCodes.RELATION_TYPE_NOT_FOUND, Resources.RELATION_TYPE_NOT_FOUND);

				if (person1Id == person2Id) throw new CustomValidationException(ErrorCodes.PERSON_CANNOT_RELATE_SELF, Resources.PERSON_CANNOT_RELATE_SELF);

				var person1 = await _repository.GetAsync(person1Id);
				if (person1 == null) throw new CustomValidationException(ErrorCodes.PERSON_NOT_FOUND, Resources.PERSON_NOT_FOUND);

				var person2 = await _repository.GetAsync(person2Id);
				if (person2 == null) throw new CustomValidationException(ErrorCodes.PERSON_NOT_FOUND, Resources.PERSON_NOT_FOUND);

				var relation = _repository.Set().FirstOrDefault(x => x.Person1ID == person1Id && x.Person2ID == person2Id);
				if (relation == null) relation = new Relation { Person1ID = person1Id, Person2ID = person2Id };

				relation.RelationTypeID = relationTypeID;
				await _repository.SaveAsync(relation);
				await _context.CommitAsync();
				await transaction.CommitAsync();

				return relation;
			}
		}

		public void DeletePersonRelation(int person1ID, int person2ID)
		{
			var relation = _repository.Set().FirstOrDefault(x => x.Person1ID == person1ID && x.Person2ID == person2ID);
			if (relation == null) throw new CustomValidationException(ErrorCodes.RELATION_NOT_FOUND, Resources.RELATION_NOT_FOUND);

			_repository.Delete(relation);
		}

		public async Task<RelationType> SaveRelationType(int id, string name)
		{
			RelationType entity = id > 0 ? (await _relationTypeRepository.GetAsync(id)
				?? throw new CustomValidationException(ErrorCodes.RELATION_TYPE_NOT_FOUND, Resources.RELATION_TYPE_NOT_FOUND))
				: new RelationType { Name = name };
			await _relationTypeRepository.SaveAsync(entity);
			await _context.CommitAsync();

			return entity;
		}

		public async Task DeleteRelationTypeAsync(int relationTypeID)
		{
			var relationType = await _relationTypeRepository.GetAsync(relationTypeID);
			if (relationType == null)
				throw new CustomValidationException(ErrorCodes.RELATION_TYPE_NOT_FOUND, Resources.RELATION_TYPE_NOT_FOUND);
			else if (_relationRepository.Set().Any(x => x.RelationTypeID == relationTypeID))
				throw new CustomValidationException(ErrorCodes.CANNOT_DELETE_RELATION_TYPE_WITH_PERSONS, Resources.CANNOT_DELETE_RELATION_TYPE_WITH_PERSONS);

			_relationTypeRepository.Delete(relationType);
		}

		public List<RelationType> GetRelationTypes()
		{
			return _relationTypeRepository.Set().ToList();
		}
	}
}