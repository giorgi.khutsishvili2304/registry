﻿using Registry.Domain;
using Registry.Domain.Interfaces.Core;
using Registry.Domain.Interfaces.Data;
using Registry.Domain.Interfaces.Services;
using Registry.Models.Persons;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using Registry.Models.Resources;
using System.Resources;
using Registry.Services.Validation;
using Registry.Domain.Enumerations;
using System.Threading.Tasks;

namespace Registry.Services
{
    public class CityService : ServiceBase<City, ICityRepository>, ICityService
    {
        private readonly IPersonRepository _personRepository;
        public CityService(IUnitOfWork context,
            ICityRepository repository,
            IPersonRepository personRepository)
            : base(context, repository)
        {
            _personRepository = personRepository;
        }

        public async Task<City> SaveCityAsync(int id, string name)
        {
            City entity = id > 0 ? (await _repository.GetAsync(id)
                ?? throw new CustomValidationException(ErrorCodes.CITY_NOT_FOUND, Resources.CITY_NOT_FOUND))
                : new City { Name = name };
            await _repository.SaveAsync(entity);
            await _context.CommitAsync();

            return entity;
        }

        public async Task DeleteCityAsync(int cityID)
        {
            var city = await _repository.GetAsync(cityID);
            if (city == null) throw new CustomValidationException(ErrorCodes.CITY_NOT_FOUND, Resources.CITY_NOT_FOUND);
            else if (_personRepository.Set().Any(x => x.CityID == cityID)) throw new CustomValidationException(ErrorCodes.CANNOT_DELETE_CITY_WITH_PERSONS, Resources.CANNOT_DELETE_CITY_WITH_PERSONS);
            _repository.Delete(city);
        }

        public List<City> GetCities()
        {
            return _repository.Set().ToList();
        }
    }
}