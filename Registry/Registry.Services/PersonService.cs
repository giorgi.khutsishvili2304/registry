﻿using Registry.Domain;
using Registry.Domain.Interfaces.Core;
using Registry.Domain.Interfaces.Data;
using Registry.Domain.Interfaces.Services;
using Registry.Models.Persons;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using Registry.Models.Resources;
using System.Resources;
using Registry.Services.Validation;
using Registry.Domain.Enumerations;
using System.Threading.Tasks;
using System.IO;
using Registry.Domain.Reports;

namespace Registry.Services
{
	public class PersonService : ServiceBase<Person, IPersonRepository>, IPersonService
	{
		private readonly ICityRepository _cityRepository;
		private readonly IContactInfoRepository _contactInfoRepository;
		private readonly IRelationRepository _relationRepository;
		private readonly IRelationTypeRepository _relationTypeRepository;
		private readonly IPersonImageRepository _personImageRepository;

		public PersonService(IUnitOfWork context,
			IPersonRepository repository,
			ICityRepository cityRepository,
			IContactInfoRepository contactInfoRepository,
			IRelationRepository relationRepository,
			IRelationTypeRepository relationTypeRepository,
			IPersonImageRepository personImageRepository
			) : base(context, repository)
		{
			_cityRepository = cityRepository;
			_contactInfoRepository = contactInfoRepository;
			_relationRepository = relationRepository;
			_relationTypeRepository = relationTypeRepository;
			_personImageRepository = personImageRepository;
		}

		public async Task<Person> SavePersonAsync(Person person)
		{
			if (_repository.Set().AsQueryable().Any(x => x.PersonalNumber == person.PersonalNumber && x.ID != person.ID))
				throw new CustomValidationException(ErrorCodes.PERSONAL_NUMBER_ALREADY_EXISTS, Resources.PERSONAL_NUMBER_ALREADY_EXISTS);

			if (!_cityRepository.Set().Any(x => x.ID == person.CityID))
				throw new CustomValidationException(ErrorCodes.CITY_NOT_FOUND, Resources.CITY_NOT_FOUND);

			if (person.ID > 0)
			{
				var entity = await _repository.GetAsync(person.ID) ?? throw new CustomValidationException(ErrorCodes.PERSON_NOT_FOUND, Resources.PERSON_NOT_FOUND);

				entity.CityID = person.CityID;
				entity.DateOfBirth = person.DateOfBirth;
				entity.GenderTypeID = person.GenderTypeID;
				entity.Firstname = person.Firstname;
				entity.Lastname = person.Lastname;
				entity.PersonalNumber = person.PersonalNumber;
				await _repository.SaveAsync(entity);
				return entity;
			}
			else
			{
				person.PersonImages = null;
				person.ContactInfos = null;
				await _repository.SaveAsync(person);
				return person;
			}
		}

		public async Task<ContactInfo> SavePersonContactInfoAsync(ContactInfo contacInfo)
		{
			var entity = await _repository.GetAsync(contacInfo.PersonID) ?? throw new CustomValidationException(ErrorCodes.PERSON_NOT_FOUND, Resources.PERSON_NOT_FOUND);
			await _contactInfoRepository.SaveAsync(contacInfo);
			return contacInfo;
		}

		public async Task DeletePersonContactInfoAsync(int id)
		{
			var contactInfo = await _contactInfoRepository.GetAsync(id);
			if (contactInfo == null) throw new CustomValidationException(ErrorCodes.CONTACT_INFO_NOT_FOUND, Resources.CONTACT_INFO_NOT_FOUND);

			_contactInfoRepository.Delete(contactInfo);
		}

		public async Task DeleteRelationAsync(int relationID)
		{
			var relation = await _relationRepository.GetAsync(relationID);
			if (relation == null) throw new CustomValidationException(ErrorCodes.RELATION_NOT_FOUND, Resources.RELATION_NOT_FOUND);

			_relationRepository.Delete(relation);
		}

		public async Task DeleteRelationTypeAsync(int relationID)
		{
			var relationType = await _relationTypeRepository.GetAsync(relationID);
			if (relationType == null) throw new CustomValidationException(ErrorCodes.RELATION_TYPE_NOT_FOUND, Resources.RELATION_TYPE_NOT_FOUND);

			_relationTypeRepository.Delete(relationType);
		}

		public async Task DeletePersonAsync(int id)
		{
			using (var transaction = await _context.BeginTransactionAsync())
			{
				var entity = await _repository.GetAsync(id) ?? throw new CustomValidationException(ErrorCodes.PERSON_NOT_FOUND, Resources.PERSON_NOT_FOUND);

				foreach (var x in entity.ContactInfos)
				{
					_contactInfoRepository.Delete(x);
				}

				foreach (var x in entity.PersonImages)
				{
					_personImageRepository.Delete(x);
				}

				_repository.Delete(entity);
				await transaction.CommitAsync();
			}
		}

		public async Task SavePersonImage(int personID, string filename, string contentType, DirectoryInfo directoryToUse, string uploadsFolderName, Stream stream)
		{
			using (var transaction = await _context.BeginTransactionAsync())
			{
				var person = await _repository.GetAsync(personID) ?? throw new CustomValidationException(ErrorCodes.PERSON_NOT_FOUND, Resources.PERSON_NOT_FOUND);

				directoryToUse = directoryToUse.CreateSubdirectory(Path.Combine("Uploads", personID.ToString()));

				var image = new PersonImage { Filename = $"Uploads/{personID}/{filename}", ContentType = contentType, Person = person };
				using (var fileStream = new FileStream(Path.Combine(directoryToUse.FullName, filename), FileMode.Create))
				{
					await stream.CopyToAsync(fileStream);
					fileStream.Close();
				}
				await _personImageRepository.SaveAsync(image);
				await transaction.CommitAsync();
			}
		}

		public List<PersonImage> GetPersonImages(int personID)
		{
			return _personImageRepository.Set().Where(x => x.PersonID == personID).ToList();
		}

		public async Task<PersonImage> GetPersonImageAsync(int id)
		{
			return await _personImageRepository.GetAsync(id);
		}

		public async Task DeletePersonImageAsync(int id)
		{
			await _personImageRepository.DeleteAsync(id);
		}

		public List<Person> SearchPersonsBasic(string firstname, string lastname, string personalNumber, int pageSize, int page)
		{
			return SearchPersonsAdvanced(firstname, lastname, personalNumber, null, null, null, null, pageSize, page);
		}

		public List<Person> SearchPersonsAdvanced(string firstname, string lastname, string personalNumber, DateTime? birthDateFrom, DateTime? birthDateTo, GenderType? genderTypeID, int? cityID, int pageSize, int page)
		{
			var data = _repository.Set();

			//gather filters
			if (!string.IsNullOrEmpty(firstname))
			{
				data = data.Where(x => x.Firstname.Contains(firstname));
			}

			if (!string.IsNullOrEmpty(lastname))
			{
				data = data.Where(x => x.Lastname.Contains(lastname));
			}

			if (!string.IsNullOrEmpty(personalNumber))
			{
				data = data.Where(x => x.PersonalNumber.Contains(personalNumber));
			}

			if (birthDateFrom.HasValue)
			{
				data = data.Where(x => x.DateOfBirth > birthDateFrom.Value);
			}

			if (birthDateTo.HasValue)
			{
				data = data.Where(x => x.DateOfBirth > birthDateTo.Value);
			}

			if (birthDateTo.HasValue)
			{
				data = data.Where(x => x.DateOfBirth > birthDateTo.Value);
			}

			if (genderTypeID.HasValue)
			{
				data = data.Where(x => x.GenderTypeID == genderTypeID.Value);
			}

			if (cityID.HasValue)
			{
				data = data.Where(x => x.CityID == cityID.Value);
			}

			var result = data.Skip((page - 1) * pageSize).Take(pageSize).ToList();//submit assembled query and take data from IQuerable
			return result;
		}

		public async Task<List<PersonRelationReportModel>> ReportForPersonsByRelation()
		{
			return await _relationRepository.ReportForPersonsByRelation();
		}

	}
}

//(სახელი, გვარი, პირადი ნომრის მიხედვით(დამთხვევა sql like პრინციპით)), დეტალური ძებნის(ყველა ველის მიხედვით) და paging-ის ფუნქციით