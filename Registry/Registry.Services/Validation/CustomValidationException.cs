﻿using Registry.Domain.Enumerations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Registry.Services.Validation
{
	public class CustomValidationException : ValidationException
	{
		public CustomValidationException(ErrorCodes errorCode, string message) : base(message)
		{
			ErrorCode = errorCode;
		}
		public ErrorCodes ErrorCode { get; set; }
	}
}