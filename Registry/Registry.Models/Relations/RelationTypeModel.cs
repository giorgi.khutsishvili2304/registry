﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Registry.Models.Relations
{
	public class RelationTypeModel
	{
		public int ID { get; set; }

		public string Name { get; set; }
	}
}