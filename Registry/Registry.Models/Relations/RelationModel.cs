﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Registry.Models.Relations
{
	public class RelationModel
	{
		public int ID { get; set; }

		public int Person1ID { get; set; }

		public int Person2ID { get; set; }

		public int RelationTypeID { get; set; }

		public string RelationTypeName { get; set; }
	}
}
