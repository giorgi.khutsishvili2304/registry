﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.Contracts;
using System.Text;

namespace Registry.Models.Attributes
{
	public class AgeValidationAttribute : ValidationAttribute
	{
		private readonly int _minAge;
		public AgeValidationAttribute(int minAge)
		{
			_minAge = minAge;
		}

		protected override ValidationResult IsValid(object value, ValidationContext validationContext)
		{
			if (IsValid(value)) return ValidationResult.Success;
			else return new ValidationResult(Resources.Resources.BirthDateValidationMessage);
		}

		public override bool IsValid(object value)
		{
			try
			{
				return Convert.ToDateTime(value).AddYears(_minAge) <= DateTime.Now;
			}
			catch { return false; }
		}
	}
}