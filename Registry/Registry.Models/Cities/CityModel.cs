﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Registry.Models.Cities
{
	public class CityModel
	{
		public int ID { get; set; }

		[Required, MaxLength(100)]
		public string Name { get; set; }
	}
}