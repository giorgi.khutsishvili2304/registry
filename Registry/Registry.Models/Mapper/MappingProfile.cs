﻿using AutoMapper;
using Registry.Domain;
using Registry.Models.Cities;
using Registry.Models.Persons;
using Registry.Models.Relations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Registry.Models.Mapper
{
	public class MappingProfile : Profile
	{
		public MappingProfile()
		{
			CreateMap<Person, PersonBaseModel>().ReverseMap();
			CreateMap<Person, PersonModel>().ReverseMap();
			CreateMap<ContactInfo, ContactInfoModel>().ReverseMap();
			CreateMap<List<ContactInfo>, List<ContactInfoModel>>().ReverseMap();

			CreateMap<Relation, RelationModel>().ReverseMap();
			CreateMap<List<Relation>, List<RelationModel>>().ReverseMap();

			CreateMap<RelationType, RelationTypeModel>().ReverseMap();
			CreateMap<List<RelationType>, List<RelationTypeModel>>().ReverseMap();

			CreateMap<City, CityModel>().ReverseMap();
			CreateMap<List<City>, List<CityModel>>().ReverseMap();

			CreateMap<PersonImage, PersonImageModel>().ReverseMap();
			CreateMap<List<PersonImage>, List<PersonImageModel>>().ReverseMap();
		}
	}
}