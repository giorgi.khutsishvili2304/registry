﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Registry.Models.Persons
{
	public class PersonImageModel
	{
		public int ID { get; set; }

		public string ContentType { get; set; }
	}
}