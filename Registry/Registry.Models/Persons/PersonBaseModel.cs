﻿using Registry.Domain.Enumerations;
using Registry.Models.Attributes;
using Registry.Models.Relations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using LocalizedMessages = Registry.Models.Resources.Resources;
namespace Registry.Models.Persons
{
	public class PersonBaseModel
	{
		public int ID { get; set; }

		[RegularExpression("^(([A-Za-z]{2,50})||([^U+10A0-U+10FF]{2,50}))$", ErrorMessageResourceName = nameof(LocalizedMessages.NameValidationMessage), ErrorMessageResourceType = typeof(LocalizedMessages))]
		public string Firstname { get; set; }

		[RegularExpression("^(([A-Za-z]{2,50})||([^U+10A0-U+10FF]{2,50}))$", ErrorMessageResourceName = nameof(LocalizedMessages.NameValidationMessage), ErrorMessageResourceType = typeof(LocalizedMessages))]
		public string Lastname { get; set; }

		[EnumDataType(typeof(GenderType), ErrorMessageResourceName = nameof(LocalizedMessages.GenderValidationMessage), ErrorMessageResourceType = typeof(LocalizedMessages))]
		public GenderType GenderTypeID { get; set; }

		[AgeValidation(18, ErrorMessageResourceName = nameof(LocalizedMessages.BirthDateValidationMessage), ErrorMessageResourceType = typeof(LocalizedMessages))]
		public DateTime DateOfBirth { get; set; }

		[Required(ErrorMessageResourceName = nameof(LocalizedMessages.CityValidationMessage), ErrorMessageResourceType = typeof(LocalizedMessages))]
		public int CityID { get; set; }

		[RegularExpression("^[0-9]{11}$", ErrorMessageResourceName = nameof(LocalizedMessages.PersonalNumberValidationMessage), ErrorMessageResourceType = typeof(LocalizedMessages))]
		public string PersonalNumber { get; set; }
	}
}