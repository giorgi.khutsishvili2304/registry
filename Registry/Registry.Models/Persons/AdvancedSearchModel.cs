﻿using Registry.Domain.Enumerations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Registry.Models.Persons
{
	public class AdvancedSearchModel : BasicSearchModel
	{
		public DateTime? BirthDateFrom { get; set; }

		public DateTime? BirthDateTo { get; set; }

		public GenderType? GenderType { get; set; }

		public int? CityID { get; set; }
	}
}