﻿using Registry.Domain.Enumerations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using LocalizedMessages = Registry.Models.Resources.Resources;

namespace Registry.Models.Persons
{
	public class ContactInfoModel
	{
		public int ID { get; set; }

		[MinLength(4, ErrorMessageResourceName = nameof(LocalizedMessages.NumberRequired), ErrorMessageResourceType = typeof(LocalizedMessages))]
		[MaxLength(50, ErrorMessageResourceName = nameof(LocalizedMessages.NumberRequired), ErrorMessageResourceType = typeof(LocalizedMessages))]
		public string Number { get; set; }

		[EnumDataType(typeof(ContactInfoType), ErrorMessageResourceName = nameof(LocalizedMessages.ContactTypeIsRequired), ErrorMessageResourceType = typeof(LocalizedMessages))]
		public ContactInfoType ContactInfoTypeID { get; set; }

		[Required]
		public int PersonID { get; set; }
	}
}