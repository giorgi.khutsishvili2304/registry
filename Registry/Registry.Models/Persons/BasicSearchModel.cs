﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Registry.Models.Persons
{
	public class BasicSearchModel
	{
		public string Firstname { get; set; }

		public string Lastname { get; set; }

		public string PersonalNumber { get; set; }

		public int Page { get; set; }

		public int PageSize { get; set; }
	}
}