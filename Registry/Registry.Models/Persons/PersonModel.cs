﻿using Registry.Domain.Enumerations;
using Registry.Models.Attributes;
using Registry.Models.Relations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using LocalizedMessages = Registry.Models.Resources.Resources;

namespace Registry.Models.Persons
{
	public class PersonModel : PersonBaseModel
	{
		public List<ContactInfoModel> ContactInfos { get; set; }

		public List<PersonImageModel> PersonImages { get; set; }

		public List<RelationModel> Relations { get; set; }
	}
}