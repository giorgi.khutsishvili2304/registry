﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Registry.Domain
{
    public class Relation
    {
        [Key]
        public int ID { get; set; }

        [Required]
        public virtual Person Person1 { get; set; }

        [Required]
        public virtual Person Person2 { get; set; }

        [Required]
        public virtual int Person1ID { get; set; }

        [Required]
        public virtual int Person2ID { get; set; }

        [Required]
        public virtual RelationType RelationType { get; set; }

        [Required]
        public int RelationTypeID { get; set; }
    }
}