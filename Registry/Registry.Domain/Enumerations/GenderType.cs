﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Registry.Domain.Enumerations
{
    public enum GenderType : byte
    {
        Male = 1,
        Female = 2
    }
}