﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Registry.Domain.Enumerations
{
    public enum ErrorCodes : int
    {
        PERSONAL_NUMBER_ALREADY_EXISTS = -10,
        PERSON_NOT_FOUND = -11,
        PERSON_CANNOT_RELATE_SELF = -12,
        CITY_NOT_FOUND = -20,
        CANNOT_DELETE_CITY_WITH_PERSONS = 21,
        CONTACT_INFO_NOT_FOUND = -30,
        RELATION_NOT_FOUND = -40,
        RELATION_TYPE_NOT_FOUND = -41,
        CANNOT_DELETE_RELATION_TYPE_WITH_PERSONS = -42,
        PAGING_PARAMETERS_INCORRECT = -50,
        FILE_IS_REQUIRED = -60,
        FILE_NOT_FOUND = -61,
        MAX_FILE_SIZE_LIMIT = -62
    }
}