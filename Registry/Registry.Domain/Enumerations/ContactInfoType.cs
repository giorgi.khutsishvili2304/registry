﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Registry.Domain.Enumerations
{
	public enum ContactInfoType : byte
	{
		Mobile = 1,
		Office = 2,
		Home = 3
	}
}