﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.Contracts;
using System.Text;

namespace Registry.Domain.ValidationAttributes
{
    public class AgeValidationAttribute : ValidationAttribute
    {
        private readonly int _minAge;
        public AgeValidationAttribute(int minAge)
        {
            _minAge = minAge;
        }

        public override bool IsValid(object value)
        {
            Contract.Requires(value != null, "Value is null");
            Contract.Requires(value.GetType() == typeof(DateTime), $"Invalid data type for {nameof(AgeValidationAttribute)}");

            return Convert.ToDateTime(value).AddYears(_minAge) >= DateTime.Now;
        }
    }
}