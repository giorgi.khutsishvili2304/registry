﻿using Registry.Domain.ValidationAttributes;
using Registry.Domain.Enumerations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Registry.Domain
{
	public class Person
	{
		[Key]
		public int ID { get; set; }

		[Required, MinLength(2), StringLength(50, MinimumLength = 2)]
		public string Firstname { get; set; }

		[Required, MinLength(2), StringLength(50, MinimumLength = 2)]
		public string Lastname { get; set; }

		[Required]
		public GenderType GenderTypeID { get; set; }

		[Required, StringLength(11, MinimumLength = 11)]
		public string PersonalNumber { get; set; }

		[DataType(DataType.Date), Required, AgeValidation(18)]
		public DateTime DateOfBirth { get; set; }

		[Required]
		public int CityID { get; set; }

		public virtual City City { get; set; }

		public virtual ICollection<Relation> Side1Relations { get; set; }
		public virtual ICollection<Relation> Side2Relations { get; set; }
		public virtual ICollection<ContactInfo> ContactInfos { get; set; }
		public virtual ICollection<PersonImage> PersonImages { get; set; }
	}
}