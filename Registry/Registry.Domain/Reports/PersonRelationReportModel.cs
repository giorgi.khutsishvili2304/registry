﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Registry.Domain.Reports
{
	public class PersonRelationReportModel
	{
		public int ID { get; set; }

		public string Firstname { get; set; }

		public string Lastname { get; set; }

		public string PersonalNumber { get; set; }

		public int Count { get; set; }

		public List<PersonRelationReportDetail> Details { get; set; } = new List<PersonRelationReportDetail>();
	}

	public class PersonRelationReportDetail
	{
		public string RelationTypeName { get; set; }

		public int RelatedPersonID { get; set; }

		public string RelatedFirstname { get; set; }

		public string RelatedLastname { get; set; }
	}
}