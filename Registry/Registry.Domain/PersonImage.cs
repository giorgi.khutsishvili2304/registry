﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Registry.Domain
{
	public class PersonImage
	{
		[Key]
		public int ID { get; set; }

		[Required, MaxLength(200)]
		public string Filename { get; set; }

		[Required, MaxLength(200)]
		public string ContentType { get; set; }

		[Required]
		public int PersonID { get; set; }

		public virtual Person Person { get; set; }
	}
}