﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Registry.Domain.Interfaces.Data
{
	public interface ICityRepository : IRepository<City>
	{
	}
}