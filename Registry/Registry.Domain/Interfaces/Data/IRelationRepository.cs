﻿using Registry.Domain.Reports;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Registry.Domain.Interfaces.Data
{
	public interface IRelationRepository : IRepository<Relation>
	{
		Task<List<PersonRelationReportModel>> ReportForPersonsByRelation();
	}
}