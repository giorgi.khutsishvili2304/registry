﻿using Registry.Domain.Interfaces.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Registry.Domain.Interfaces.Data
{
	public interface IRepository<TEntity> where TEntity : class
	{
		Task<TEntity> GetAsync(int id);
		IQueryable<TEntity> Set();
		Task SaveAsync(TEntity entity);
		Task DeleteAsync(int id);
		void Delete(TEntity entity);
	}
}