﻿using Registry.Domain.Enumerations;
using Registry.Domain.Reports;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Registry.Domain.Interfaces.Services
{
	public interface IPersonService : IService<Person>
	{
		Task<Person> SavePersonAsync(Person person);
		Task<ContactInfo> SavePersonContactInfoAsync(ContactInfo contacInfo);
		Task DeletePersonContactInfoAsync(int id);
		Task DeletePersonAsync(int id);
		Task SavePersonImage(int personID, string filename, string contentType, DirectoryInfo directoryToStore, string uploadsFolderName, Stream stream);
		List<PersonImage> GetPersonImages(int personID);
		Task<PersonImage> GetPersonImageAsync(int id);
		Task DeletePersonImageAsync(int id);
		List<Person> SearchPersonsBasic(string firstname, string lastname, string personalNumber, int pageSize, int page);
		List<Person> SearchPersonsAdvanced(string firstname, string lastname, string personalNumber, DateTime? birthDateFrom, DateTime? birthDateTo, GenderType? genderTypeID, int? cityID, int pageSize, int page);
		Task<List<PersonRelationReportModel>> ReportForPersonsByRelation();
	}
}