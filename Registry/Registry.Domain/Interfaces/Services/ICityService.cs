﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
namespace Registry.Domain.Interfaces.Services
{
	public interface ICityService : IService<City>
	{
		List<City> GetCities();
		Task<City> SaveCityAsync(int id, string name);
		Task DeleteCityAsync(int relationTypeID);
	}
}