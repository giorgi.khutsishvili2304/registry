﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Registry.Domain.Interfaces.Services
{
	public interface IService<T> where T : class, new()
	{
		Task<T> GetAsync(int id);
		IQueryable<T> Set();
		Task SaveAsync(T entity);
		Task DeleteAsync(int id);
		Task DeleteAsync(T entity);
	}
}