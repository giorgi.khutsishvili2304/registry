﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
namespace Registry.Domain.Interfaces.Services
{
	public interface IRelationService : IService<Relation>
	{
		Task<Relation> SavePersonRelationAsync(int person1Id, int person2Id, int relationTypeID);
		void DeletePersonRelation(int person1ID, int person2ID);
		List<RelationType> GetRelationTypes();
		Task<RelationType> SaveRelationType(int id, string name);
		Task DeleteRelationTypeAsync(int relationTypeID);
	}
}