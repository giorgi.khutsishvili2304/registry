﻿using Registry.Domain.Enumerations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Registry.Domain
{
	public class ContactInfo
	{
		[Key]
		public int ID { get; set; }

		[Required]
		public ContactInfoType ContactInfoTypeID { get; set; }

		[Required, MinLength(4), MaxLength(50)]
		public string Number { get; set; }

		[Required]
		public int PersonID { get; set; }

		public virtual Person Person { get; set; }
	}
}