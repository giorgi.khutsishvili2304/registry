﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Configuration;
using Registry.Domain;
using Registry.Domain.Interfaces.Core;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Registry.Repositories.Context
{
	public class RegistryDbContext : DbContext, IUnitOfWork
	{

		private readonly IConfiguration _configuration;

		public RegistryDbContext() { }

		public RegistryDbContext(IConfiguration configuration)
		{
			_configuration = configuration;
		}

		public RegistryDbContext(DbContextOptions<RegistryDbContext> options) : base(options) { }

		public async Task<IDbContextTransaction> BeginTransactionAsync()
		{
			return await Database.BeginTransactionAsync();
		}

		public virtual DbSet<Person> People { get; set; }
		public virtual DbSet<City> Cities { get; set; }
		public virtual DbSet<Relation> Relations { get; set; }
		public virtual DbSet<RelationType> RelationTypes { get; set; }
		public virtual DbSet<ContactInfo> ContactInfos { get; set; }
		public virtual DbSet<PersonImage> PersonImages { get; set; }

		public async Task CommitAsync()
		{
			await SaveChangesAsync();
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Person>().HasMany(x => x.Side1Relations).WithOne(x => x.Person1)
					.HasForeignKey(x => x.Person1ID);

			modelBuilder.Entity<Person>().HasMany(x => x.Side2Relations).WithOne(x => x.Person2)
					.HasForeignKey(x => x.Person2ID);

			modelBuilder.Entity<Person>().HasMany(x => x.ContactInfos).WithOne(x => x.Person)
				.HasForeignKey(x => x.PersonID);

			modelBuilder.Model.GetEntityTypes().SelectMany(t => t.GetForeignKeys()).ToList()
				.ForEach(x => x.DeleteBehavior = DeleteBehavior.Restrict);

			base.OnModelCreating(modelBuilder);
		}

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			string connectionString = null;

//#if MIGRATION
//			connectionString = "Server=V8\\SQLEXPRESS2016; Database=Registry; Integrated Security=true;";
//#else
			connectionString = _configuration.GetConnectionString(nameof(RegistryDbContext)) 
			?? throw new ConfigurationErrorsException($"Connection string with the name {nameof(RegistryDbContext)} not found!");
//#endif

			optionsBuilder.UseLazyLoadingProxies().UseSqlServer(connectionString);
			base.OnConfiguring(optionsBuilder);
		}
	}
}