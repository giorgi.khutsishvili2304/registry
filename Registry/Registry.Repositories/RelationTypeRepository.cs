﻿using Registry.Domain;
using Registry.Domain.Interfaces.Core;
using Registry.Domain.Interfaces.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Registry.Repositories
{
	public class RelationTypeRepository : RepositoryBase<RelationType>, IRelationTypeRepository
	{
		public RelationTypeRepository(IUnitOfWork context) : base(context) { }
	}
}