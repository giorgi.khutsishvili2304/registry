﻿using Registry.Domain;
using Registry.Domain.Interfaces.Core;
using Registry.Domain.Interfaces.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Registry.Repositories
{
	public class CityRepository : RepositoryBase<City>, ICityRepository
	{
		public CityRepository(IUnitOfWork context) : base(context) { }
	}
}