﻿using Microsoft.EntityFrameworkCore;
using Registry.Domain.Interfaces.Core;
using Registry.Domain.Interfaces.Data;
using Registry.Repositories.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Registry.Repositories
{
	public abstract class RepositoryBase<T> : IRepository<T> where T : class, new()
	{

		protected RegistryDbContext _context;

		public IUnitOfWork Context
		{
			get { return _context; }
		}

		internal RepositoryBase(IUnitOfWork context) : this(context as RegistryDbContext) { }

		internal RepositoryBase(RegistryDbContext context)
		{
			_context = context ?? throw new ArgumentNullException("context");
		}

		public virtual async Task<T> GetAsync(int id)
		{
			return await _context.Set<T>().FindAsync(id);
		}

		public virtual IQueryable<T> Set()
		{
			return _context.Set<T>();
		}

		public virtual async Task SaveAsync(T entity)
		{
			await SaveAsync(_context.Set<T>(), entity);
			await _context.SaveChangesAsync();
		}

		public async Task DeleteAsync(int id)
		{
			Delete(await GetAsync(id));
			await _context.SaveChangesAsync();
		}

		public virtual void Delete(T entity)
		{
			Delete(_context.Set<T>(), entity);
			_context.SaveChangesAsync().Wait();
		}

		protected virtual async Task SaveAsync(DbSet<T> set, T entity)
		{
			var entry = _context.Entry(entity);
			if (entry == null || entry.State == EntityState.Detached) await set.AddAsync(entity);
		}

		protected virtual void Delete(DbSet<T> set, T entity)
		{
			set.Remove(entity);
			_context.SaveChangesAsync().Wait();
		}
	}
}