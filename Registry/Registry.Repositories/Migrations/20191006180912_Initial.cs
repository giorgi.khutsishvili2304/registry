﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Registry.Repositories.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Cities",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cities", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "RelationTypes",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RelationTypes", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "People",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Firstname = table.Column<string>(maxLength: 50, nullable: false),
                    Lastname = table.Column<string>(maxLength: 50, nullable: false),
                    GenderTypeID = table.Column<byte>(nullable: false),
                    PersonalNumber = table.Column<string>(maxLength: 11, nullable: false),
                    DateOfBirth = table.Column<DateTime>(nullable: false),
                    CityID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_People", x => x.ID);
                    table.ForeignKey(
                        name: "FK_People_Cities_CityID",
                        column: x => x.CityID,
                        principalTable: "Cities",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ContactInfos",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ContactInfoTypeID = table.Column<byte>(nullable: false),
                    Number = table.Column<string>(maxLength: 50, nullable: false),
                    PersonID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContactInfos", x => x.ID);
                    table.ForeignKey(
                        name: "FK_ContactInfos_People_PersonID",
                        column: x => x.PersonID,
                        principalTable: "People",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PersonImages",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Filename = table.Column<string>(maxLength: 200, nullable: false),
                    ContentType = table.Column<string>(maxLength: 200, nullable: false),
                    PersonID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PersonImages", x => x.ID);
                    table.ForeignKey(
                        name: "FK_PersonImages_People_PersonID",
                        column: x => x.PersonID,
                        principalTable: "People",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Relations",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Person1ID = table.Column<int>(nullable: false),
                    Person2ID = table.Column<int>(nullable: false),
                    RelationTypeID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Relations", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Relations_People_Person1ID",
                        column: x => x.Person1ID,
                        principalTable: "People",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Relations_People_Person2ID",
                        column: x => x.Person2ID,
                        principalTable: "People",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Relations_RelationTypes_RelationTypeID",
                        column: x => x.RelationTypeID,
                        principalTable: "RelationTypes",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ContactInfos_PersonID",
                table: "ContactInfos",
                column: "PersonID");

            migrationBuilder.CreateIndex(
                name: "IX_People_CityID",
                table: "People",
                column: "CityID");

            migrationBuilder.CreateIndex(
                name: "IX_PersonImages_PersonID",
                table: "PersonImages",
                column: "PersonID");

            migrationBuilder.CreateIndex(
                name: "IX_Relations_Person1ID",
                table: "Relations",
                column: "Person1ID");

            migrationBuilder.CreateIndex(
                name: "IX_Relations_Person2ID",
                table: "Relations",
                column: "Person2ID");

            migrationBuilder.CreateIndex(
                name: "IX_Relations_RelationTypeID",
                table: "Relations",
                column: "RelationTypeID");

					migrationBuilder.Sql(@"
CREATE PROCEDURE dbo.spGetPersonRelationReport
AS
BEGIN
    SELECT P.ID,
	   P.Firstname Firstname,
     P.Lastname Lastname,
     P.PersonalNumber PersonalNumber,
	   RT.ID RelationTypeID,
	   RT.Name RelationTypeName,
	   COUNT(R.ID) OVER (PARTITION BY P.ID, R.RelationTypeID) [Count],
	   PP.ID RelatedPersonID,
	   PP.Firstname RelatedFirstname,
	   PP.Lastname RelatedLastname
	FROM dbo.People P
		LEFT JOIN dbo.Relations R ON R.Person1ID = P.ID
		LEFT JOIN dbo.RelationTypes RT ON RT.ID = R.RelationTypeID
		LEFT JOIN dbo.People PP ON R.Person2ID = PP.ID
END");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ContactInfos");

            migrationBuilder.DropTable(
                name: "PersonImages");

            migrationBuilder.DropTable(
                name: "Relations");

            migrationBuilder.DropTable(
                name: "People");

            migrationBuilder.DropTable(
                name: "RelationTypes");

            migrationBuilder.DropTable(
                name: "Cities");
        }
    }
}
