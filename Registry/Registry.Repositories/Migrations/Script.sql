﻿USE master

CREATE DATABASE Registry

GO

USE Registry

IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;

GO

CREATE TABLE [Cities] (
    [ID] int NOT NULL IDENTITY,
    [Name] nvarchar(100) NOT NULL,
    CONSTRAINT [PK_Cities] PRIMARY KEY ([ID])
);

GO

CREATE TABLE [RelationTypes] (
    [ID] int NOT NULL IDENTITY,
    [Name] nvarchar(100) NOT NULL,
    CONSTRAINT [PK_RelationTypes] PRIMARY KEY ([ID])
);

GO

CREATE TABLE [People] (
    [ID] int NOT NULL IDENTITY,
    [Firstname] nvarchar(50) NOT NULL,
    [Lastname] nvarchar(50) NOT NULL,
    [GenderTypeID] tinyint NOT NULL,
    [PersonalNumber] nvarchar(11) NOT NULL,
    [DateOfBirth] datetime2 NOT NULL,
    [CityID] int NOT NULL,
    CONSTRAINT [PK_People] PRIMARY KEY ([ID]),
    CONSTRAINT [FK_People_Cities_CityID] FOREIGN KEY ([CityID]) REFERENCES [Cities] ([ID]) ON DELETE NO ACTION
);

GO

CREATE TABLE [ContactInfos] (
    [ID] int NOT NULL IDENTITY,
    [ContactInfoTypeID] tinyint NOT NULL,
    [Number] nvarchar(50) NOT NULL,
    [PersonID] int NOT NULL,
    CONSTRAINT [PK_ContactInfos] PRIMARY KEY ([ID]),
    CONSTRAINT [FK_ContactInfos_People_PersonID] FOREIGN KEY ([PersonID]) REFERENCES [People] ([ID]) ON DELETE NO ACTION
);

GO

CREATE TABLE [PersonImages] (
    [ID] int NOT NULL IDENTITY,
    [Filename] nvarchar(200) NOT NULL,
    [ContentType] nvarchar(200) NOT NULL,
    [PersonID] int NOT NULL,
    CONSTRAINT [PK_PersonImages] PRIMARY KEY ([ID]),
    CONSTRAINT [FK_PersonImages_People_PersonID] FOREIGN KEY ([PersonID]) REFERENCES [People] ([ID]) ON DELETE NO ACTION
);

GO

CREATE TABLE [Relations] (
    [ID] int NOT NULL IDENTITY,
    [Person1ID] int NOT NULL,
    [Person2ID] int NOT NULL,
    [RelationTypeID] int NOT NULL,
    CONSTRAINT [PK_Relations] PRIMARY KEY ([ID]),
    CONSTRAINT [FK_Relations_People_Person1ID] FOREIGN KEY ([Person1ID]) REFERENCES [People] ([ID]) ON DELETE NO ACTION,
    CONSTRAINT [FK_Relations_People_Person2ID] FOREIGN KEY ([Person2ID]) REFERENCES [People] ([ID]) ON DELETE NO ACTION,
    CONSTRAINT [FK_Relations_RelationTypes_RelationTypeID] FOREIGN KEY ([RelationTypeID]) REFERENCES [RelationTypes] ([ID]) ON DELETE NO ACTION
);

GO

CREATE INDEX [IX_ContactInfos_PersonID] ON [ContactInfos] ([PersonID]);

GO

CREATE INDEX [IX_People_CityID] ON [People] ([CityID]);

GO

CREATE INDEX [IX_PersonImages_PersonID] ON [PersonImages] ([PersonID]);

GO

CREATE INDEX [IX_Relations_Person1ID] ON [Relations] ([Person1ID]);

GO

CREATE INDEX [IX_Relations_Person2ID] ON [Relations] ([Person2ID]);

GO

CREATE INDEX [IX_Relations_RelationTypeID] ON [Relations] ([RelationTypeID]);

GO
 
CREATE PROCEDURE dbo.spGetPersonRelationReport
AS
BEGIN
    SELECT P.ID,
	   P.Firstname Firstname,
     P.Lastname Lastname,
     P.PersonalNumber PersonalNumber,
	   RT.ID RelationTypeID,
	   RT.Name RelationTypeName,
	   COUNT(R.ID) OVER (PARTITION BY P.ID, R.RelationTypeID) [Count],
	   PP.ID RelatedPersonID,
	   PP.Firstname RelatedFirstname,
	   PP.Lastname RelatedLastname
	FROM dbo.People P
		LEFT JOIN dbo.Relations R ON R.Person1ID = P.ID
		LEFT JOIN dbo.RelationTypes RT ON RT.ID = R.RelationTypeID
		LEFT JOIN dbo.People PP ON R.Person2ID = PP.ID
END

GO 

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20191006180912_Initial', N'3.0.0');

INSERT INTO dbo.Cities
(Name)
VALUES
(N'თბილისი'),
(N'ბათუმი'),
(N'ქუთაისი') 

INSERT INTO dbo.RelationTypes 
(Name)
VALUES
(N'კოლეგა'),
(N'ნაცნობი'),
(N'ნათესავი')  
