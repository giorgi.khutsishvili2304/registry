﻿using Microsoft.EntityFrameworkCore;
using Registry.Domain;
using Registry.Domain.Interfaces.Core;
using Registry.Domain.Interfaces.Data;
using Registry.Domain.Reports;
using Registry.Repositories.Context;
using System;
using System.Linq;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using System.Data.Common;

namespace Registry.Repositories
{
	public class RelationRepository : RepositoryBase<Relation>, IRelationRepository
	{
		public RelationRepository(IUnitOfWork context) : base(context) { }

		public async Task<List<PersonRelationReportModel>> ReportForPersonsByRelation()
		{
			ConcurrentDictionary<int, PersonRelationReportModel> data = new ConcurrentDictionary<int, PersonRelationReportModel>();

			_context.Database.OpenConnection();

			using (var cmd = _context.Database.GetDbConnection().CreateCommand())
			{
				cmd.CommandType = CommandType.StoredProcedure;
				cmd.CommandText = "dbo.spGetPersonRelationReport";

				using (var dr = await cmd.ExecuteReaderAsync())
				{
					while (await dr.ReadAsync())
					{
						PersonRelationReportModel person = new PersonRelationReportModel();
						person.ID = dr.GetInt32(nameof(person.ID));
						person.Firstname = dr.GetString(nameof(person.Firstname));
						person.Lastname = dr.GetString(nameof(person.Lastname));
						person.PersonalNumber = dr.GetString(nameof(person.PersonalNumber));
						person.Count = dr.GetInt32(nameof(person.Count));

						data.TryAdd(person.ID, person);

						//relation count
						if (person.Count == 0) continue;

						var detail = new PersonRelationReportDetail();
						detail.RelationTypeName = dr.GetString(nameof(detail.RelationTypeName));
						detail.RelatedPersonID = dr.GetInt32(nameof(detail.RelatedPersonID));
						detail.RelatedFirstname = dr.GetString(nameof(detail.RelatedFirstname));
						detail.RelatedLastname = dr.GetString(nameof(detail.RelatedLastname));

						data[person.ID].Details.Add(detail);
					}
				}
			}

			return data.Values.ToList();
		}
	}
}