﻿using Registry.Domain;
using Registry.Domain.Interfaces.Core;
using Registry.Domain.Interfaces.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Registry.Repositories
{
	public class PersonRepository : RepositoryBase<Person>, IPersonRepository
	{
		public PersonRepository(IUnitOfWork context) : base(context) { }
	}
}