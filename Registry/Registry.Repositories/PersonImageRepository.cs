﻿using Registry.Domain;
using Registry.Domain.Interfaces.Core;
using Registry.Domain.Interfaces.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Registry.Repositories
{
	public class PersonImageRepository : RepositoryBase<PersonImage>, IPersonImageRepository
	{
		public PersonImageRepository(IUnitOfWork context) : base(context) { }
	}
}