﻿using System;
using System.Collections.Generic;
using System.Text;
using Autofac;
using Registry.Domain.Interfaces.Core;
using Registry.Domain.Interfaces.Data;
using Registry.Domain.Interfaces.Services;
using Registry.Repositories;
using Registry.Repositories.Context;
using Registry.Services;

namespace Registry.Infrastructure.DI
{
	public class RegistryInjectionModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			builder.RegisterType<RegistryDbContext>().As<IUnitOfWork>().InstancePerLifetimeScope();

			builder.RegisterType<PersonRepository>().As<IPersonRepository>();
			builder.RegisterType<ContactInfoRepository>().As<IContactInfoRepository>();
			builder.RegisterType<CityRepository>().As<ICityRepository>();
			builder.RegisterType<RelationRepository>().As<IRelationRepository>();
			builder.RegisterType<RelationTypeRepository>().As<IRelationTypeRepository>();
			builder.RegisterType<CityRepository>().As<ICityRepository>();
			builder.RegisterType<PersonImageRepository>().As<IPersonImageRepository>();

			builder.RegisterType<PersonService>().As<IPersonService>();
			builder.RegisterType<RelationService>().As<IRelationService>();
			builder.RegisterType<CityService>().As<ICityService>();

			base.Load(builder);
		}
	}
}